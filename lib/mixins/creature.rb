module Creature

	def build_creatures(window)
		sprite = Gosu::Image.load_tiles(window,"resources/img/actor/creatures.png",32,32,true)
		
		@creature_collection = Hash.new
		@creature_collection[:spacemummy] = [sprite[0],sprite[1],sprite[2]]
	  @creature_collection[:tantank] = [sprite[4],sprite[5],sprite[6]]
	  @creature_collection[:robotdrone] = [sprite[8],sprite[9],sprite[10]]
	  @creature_collection[:robot] = [sprite[12],sprite[13],sprite[14]]
	  @creature_collection[:humvee] = [sprite[16],sprite[17],sprite[18]]
	  @creature_collection[:beetle] = [sprite[20],sprite[21],sprite[22]]
	  @creature_collection[:greenalien] = [sprite[24],sprite[25],sprite[26]]
	  @creature_collection[:robotmummy] = [sprite[28],sprite[29],sprite[30]]
	  @creature_collection[:pindrone] = [sprite[32],sprite[33],sprite[34]]
	  @creature_collection[:martialartist] = [sprite[36],sprite[37],sprite[38]]
	  @creature_collection[:wizard] = [sprite[40],sprite[41],sprite[42]]
	  @creature_collection[:tankdrone] = [sprite[44],sprite[45],sprite[46]]
	  @creature_collection[:skulldrone] = [sprite[48],sprite[49],sprite[50]]
	  @creature_collection[:greentank] = [sprite[52],sprite[53],sprite[54]]
	  @creature_collection[:eye] = [sprite[56],sprite[57],sprite[58]]
	  @creature_collection[:greensludge] = [sprite[60],sprite[61],sprite[62]]
	  @creature_collection[:redsludge] = [sprite[64],sprite[65],sprite[66]]
	  @creature_collection[:headalien] = [sprite[68],sprite[69],sprite[70]]
	  @creature_collection[:cyborg] = [sprite[72],sprite[73],sprite[74]]
	end

	def creature(type)
		return @creature_collection[type].concat(@creature_collection[type].reverse)
	end

end