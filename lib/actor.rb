require_relative 'mixins/creature'
require_relative 'mixins/zorder'
class Actor
include Creature
include ZOrder

	def initialize(window,type)
		build_creatures(window)
    @animation = creature(type)
    @x,@y = 96,96
	end

	def draw
		@animation[Gosu::milliseconds / 125 % @animation.size].draw(@x,@y,ZOrder::Actor)
	end

end