##Defines a tile-based map in spacejacket
#Handles collision, debugging and displaying the overall background of the world
require 'gosu'
require 'json'
require_relative 'mixins/zorder'

class Map
include ZOrder

	attr_accessor :collision
	NORMAL_COLOR = Gosu::Color::WHITE
	GRID_COLOR = Gosu::Color::YELLOW
	DEBUG_COLOR = Gosu::Color::WHITE
	COLLISION_COLOR = Gosu::Color.rgba(0,0,255,75)


	def initialize(window,source,enable_collisionmap = true,enable_grid = false)
		@enable_collisionmap = enable_collisionmap
		@enable_grid = enable_grid

		@window = window
		@map = load_map(source)
		@width, @height = @map["width"], @map["height"]
		@tilewidth, @tileheight = @map["tilewidth"], @map["tileheight"]
		@tileset = Gosu::Image.load_tiles(window, @map["tilesets"].first["image"], @tilewidth, @tileheight, true)
		@font = Gosu::Font.new(window,"Verdana",12)

		@collision = []
	end

	#draws the map to the window
  def draw
		@height.times do |y|
    	@width.times do |x|
      	tile =  y * @width + x
       	xcord = x*@tilewidth +50 #TODO Remove + at end (added to center map on background) 
       	ycord = y*@tileheight +40 #TODO Remove + at end (added to center map on background) 

       	@map['layers'].each do |layer|
         tileimage = layer['data'][tile] -1
          if tileimage > 0
           	if layer["name"] == "collision"
           		@tileset[tileimage].draw(xcord,ycord,ZOrder::Collision)
           		collision[tile] = true
           		if @enable_collisionmap then draw_collision_map(xcord,ycord) end
    	    	else
    	    		@tileset[tileimage].draw(xcord,ycord,ZOrder::Terrain)
    	    	end  
          end
        end
        if @enable_grid then draw_grid(xcord,ycord) end
     	end
   	end
  end

	private
		#loads a map from json
	  def load_map(filename)
	  	JSON.parse(IO.read("resources/maps/#{filename}.json"))
	  end

	  def draw_collision_map(xcord,ycord)
		    #highlight collisionmap
				@window.draw_quad(xcord+@tilewidth,ycord+@tileheight,COLLISION_COLOR,
												xcord,ycord+@tileheight,COLLISION_COLOR,
												xcord+@tilewidth,ycord,COLLISION_COLOR,
												xcord,ycord,COLLISION_COLOR,ZOrder::Debug)
	  end

	  def draw_grid(xcord,ycord)
	  	#Draw a grid for each tile
    	@window.draw_line(xcord+@tilewidth,ycord+@tileheight,GRID_COLOR,xcord,ycord+@tileheight,GRID_COLOR,ZOrder::Debug)
     	@window.draw_line(xcord+@tilewidth,ycord+@tileheight,GRID_COLOR,xcord+@tilewidth,ycord,GRID_COLOR,ZOrder::Debug)
     	@window.draw_line(xcord+@tilewidth,ycord,GRID_COLOR,xcord,ycord,GRID_COLOR,ZOrder::Debug)
     	@window.draw_line(xcord,ycord + @tileheight,GRID_COLOR,xcord,ycord,GRID_COLOR,ZOrder::Debug)
     	@font.draw("#{xcord/@tilewidth},#{ycord/@tileheight}",xcord,ycord,ZOrder::Debug,1,1,DEBUG_COLOR)
	  end

end
