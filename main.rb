require 'gosu'
require 'json'
require_relative 'lib/map'
require_relative 'lib/actor'

class MyWindow < Gosu::Window
  def initialize
    super(900, 700, false)
    self.caption = "Spacejacket"
    @map = Map.new(self,"battleground/forest")

    @actor = Actor.new(self,:robotmummy)
    #@creatures = Gosu::Image.load_tiles(self,"resources/img/actor/creatures.png",32,32,true)
    #@animation = [@creatures[12],@creatures[13],@creatures[14],@creatures[14],@creatures[13],@creatures[12]]
    #@animation = [@creatures[68],@creatures[69],@creatures[70],@creatures[70],@creatures[69],@creatures[68]]
  end
  
  def update
  end
  
  def draw
    @map.draw
    @actor.draw
  end

end

window = MyWindow.new
window.show